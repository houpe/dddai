<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return 'welcome';
});
Route::get('msg','MsgController@Index');
Route::post('create','MsgController@Create');
Route::get('page/{page?}','MsgController@Page');
Route::get('insert','MsgController@Insert');

Route::get('member/insert','MemberController@insert');
Route::get('member/orm1','MemberController@orm1');

Route::get('member/info','MemberController@info');
Route::get('member/delete/{id?}','MemberController@delete');
Route::get('member/{id?}','MemberController@info')->where('id','[0-9]+');

 