<?php
/**
 * Created by PhpStorm.
 * User: houpeng
 * Date: 2018/9/19
 * Time: 上午9:48
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    protected $table = 'logs';
    protected $primaryKey = 'id';

}